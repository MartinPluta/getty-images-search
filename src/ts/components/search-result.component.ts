import { Component, OnInit } from '@angular/core';
import { Image } from '../model/image';
import { SearchService } from '../services/search.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'search-result',
    templateUrl: '../../html/search-result.component.html',
    styleUrls: ['../../css/search-result.component.css', '../../css/imagehover/imagehover.min.css']
})

export class SearchResultComponent implements OnInit {
    private images: Array<Image>;
    public searchTerm: string = null;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private imagesService: SearchService
    ) {}

    ngOnInit(): void {
      this.route.params.forEach((params: Params) => {
        let searchTerm = params['searchTerm'];
        this.getImages(searchTerm);
      });
    }

    private getImages(searchTerm: string): void {
      this.imagesService.getImages(searchTerm)
            .subscribe(res => this.images = res);
      this.setSearchTerm(searchTerm);
    }

    private setSearchTerm(searchTerm: string) {
      this.searchTerm = searchTerm;
    }
}
